---?color=linear-gradient(to right, #c02425, #f0cb35)
@title[Introduction]

@snap[west text-25 text-bold text-white]
GitPitch<br>*The Template*
@snapend

@snap[south-west byline text-white text-06]
The Fastest Way From Idea To Presentation.
@snapend

---
@title[Slide Markdown]

### Each slide in this presentation is provided as a *template*.

<br><br>

@snap[south span-100 text-purple text-05]
Reuse the *markdown snippet* for any slide in this template within your own @css[text-gold text-bold](PITCHME.md) files.
@snapend

---
@title[Tip! Fullscreen]

![TIP](assets/img/tip.png)
<br>
For the best viewing experience, press F for fullscreen.
@css[template-note](We recommend using the *SPACE* key to navigate between slides.)

---?include=assets/md/split-screen/PITCHME.md

---?include=assets/md/sidebar/PITCHME.md

---?include=assets/md/list-content/PITCHME.md

---?include=assets/md/boxed-text/PITCHME.md

---?include=assets/md/image/PITCHME.md

---?include=assets/md/sidebox/PITCHME.md

---?include=assets/md/code-presenting/PITCHME.md

---?include=assets/md/header-footer/PITCHME.md

---?include=assets/md/quotation/PITCHME.md

---?include=assets/md/announcement/PITCHME.md

---?include=assets/md/about/PITCHME.md

---?include=assets/md/wrap-up/PITCHME.md

---?image=assets/img/presenter.jpg
@title[The Template Docs]

@snap[north-west sign-off]
### **Now it's @color[#e58537](your) turn.**
<br>
#### Quickstart your next slide deck<br>with @size[1.4em](The GitPitch Template).
@snapend

@snap[south docslink text-gold span-100]
For supporting documentation see the [The Template Docs](https://gitpitch.com/docs/the-template)
@snapend

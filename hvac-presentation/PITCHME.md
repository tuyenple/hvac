---?image=assets/img/cover.jpg&size=100% 100%
@snap[midpoint span-65]
@box[bg-blue text-white rounded](**RL-based HVAC control systems**#Building an HVAC control systems using deep reinforcement learning)
@snapend

---
**Outlines**
<br><br>
Overview
<br><br>
Motivation
<br><br>
Related works
<br><br>
Proposed ideas
<br><br>
Results

---
**Overview**

+++?image=assets/img/hvac.jpg&size=100% 100%
@snap[west span-60 fragment]
@box[bg-white text-black](HVAC systems#stands for **Heating**, **Ventilation** and **Air Conditioning** and is responsible for keeping us warm and comfortable indoors)
@snapend

+++?image=assets/img/consume_1.png&position=center&size=auto 70%
@snap[south span-100]
HVAC takes up 50% of the energy in a building [1]
@snapend
@snap[north-west template-note text-gray]
[[1] https://www.eia.gov/consumption/residential/](https://www.eia.gov/consumption/residential/)
@snapend

+++?image=assets/img/consume_2.png&position=left&size=50% auto
@snap[east span-50]
@quote[, and electricity is used in all sectors (IEA)]
@snapend
@snap[north-west template-note text-gray]
[(IEA) https://www.iea.org/](https://www.iea.org/)
@snapend

---
**Motivation**

+++?image=assets/img/demand.png&size=auto 70%
@snap[south span-100]
Cooling and heating is the fastest growing use of energy in buildings (IEA)
@snapend
@snap[north-west template-note text-gray]
[(IEA) https://www.iea.org/futureofcooling/](https://www.iea.org/futureofcooling/)
@snapend

+++
**As a result ...**

+++
@snap[west]
@img[span-100](assets/img/electricity_prices.jpeg)
@snapend
@snap[east span-50 fragment]
@quote[Price response flexible demand  (KEPCO)]
@snapend
@snap[south-west template-note text-gray]
[(KEPCO) http://www.theinvestor.co.kr/view.php?ud=20160808000941](http://www.theinvestor.co.kr/view.php?ud=20160808000941)
@snapend

+++?image=/assets/img/climate.png&size=100% 100%&position=center
@snap[south span-50 fragment]
@quote[Global climate change due to $CO_2$ emission]
@snapend

+++
**An efficient HVAC control systems can reduce the energy consumption and $CO_2$ emission**

+++?image=/assets/img/modern_hvac.png&size=auto 80%
@snap[south span-100 fragment]
@box[bg-blue text-white](**Nowadays**#HVAC control systems becomes complex and complicated due to the increasing of input data in a modern building (E.g. weather, occupancy, equipments, ...))
@snapend

+++?image=/assets/img/mdp.png&size=auto 80%
@snap[midpoint span-60 fragment]
@box[bg-blue text-white](#Deep RL is a framework to build intelligence controllers that can adapt with environments)
@snapend

---
**Related works**

+++?image=/assets/img/hvac_methods.png&size=auto 70%
@snap[south span-100]
Overall HVAC control methods [1]
@snapend
@snap[north-west template-note text-gray]
[[1] Behrooz, Farinaz, et al. "Review of Control Techniques for HVAC Systems—Nonlinearity Approaches Based on Fuzzy Cognitive Maps." Energies 11.3 (2018): 495.](https://www.mdpi.com/1996-1073/11/3/495)
@snapend

+++?image=/assets/img/on_off.png&size=auto 70%&position=center
@snap[north-west span-100]
**On-Off**
@snapend
@snap[south span-100]
Maximum or Zero
@snapend
@snap[west span-40 fragment]
@box[bg-blue text-white](Pros#low initial cost and simple)
@snapend
@snap[east span-40 fragment]
@box[bg-blue text-white](Cons#+ not accurate <br> + SISO)
@snapend

+++?image=/assets/img/pid.png&size=auto 70%
@snap[north-west span-100]
**PID**
@snapend
@snap[south span-100]
Proportional, Integral, Derivative (PID)
@snapend
@snap[west span-40 fragment]
@box[bg-blue text-white](Pros#+ widely use)
@snapend
@snap[east span-40 fragment]
@box[bg-blue text-white](Cons#+ Hard to tune<br> + Not proper for non-linear systems <br> + SISO)
@snapend

+++
**, and others (optimal control, robust control, ...) [1]**

@snap[south-west template-note text-gray]
[[1] Behrooz, Farinaz, et al. "Review of Control Techniques for HVAC Systems—Nonlinearity Approaches Based on Fuzzy Cognitive Maps." Energies 11.3 (2018): 495.](https://www.mdpi.com/1996-1073/11/3/495)
@snapend

+++

@snap[north-west span-40 fragment]
@box[bg-green text-white rounded demo-box-step-padding](1. Non-linear#Can deal highly non-linear problem as HVAC system)
@snapend

@snap[north-east span-40 fragment]
@box[bg-orange text-white rounded demo-box-step-padding](2. Personalized#Adapt with specified buildings)
@snapend

@snap[south-east span-40 fragment]
@box[bg-pink text-white rounded demo-box-step-padding](3. Big data#It uses various kind of input to control the system)
@snapend

@snap[south-west span-40 fragment]
@box[bg-blue text-white rounded demo-box-step-padding](4. Continuous Learning#RL-based HVAC control systems are learned periodically)
@snapend

@snap[midpoint span-20]
**Why Deep RL?**
@snapend

+++
**Survey on studies about RL-based energy control systems from 2015 to 2019. I see that ...**

+++
**Surveys**

@ul[list-bullets-circles]
- Before 2016, they use classical RL methods (Q-learning, SARSA, Fitted Q-iteration)
- From 2017, they start to apply deep RL
- Which Deep RL methods:
    - A2C, A3C, DQN, DDQN, DDPG [1][2][3]
@ulend
@snap[south-west template-note text-gray]
[[1] Wei, Tianshu, Yanzhi Wang, and Qi Zhu. "Deep reinforcement learning for building HVAC control." Proceedings of the 54th Annual Design Automation Conference 2017. ACM, 2017.](https://ieeexplore.ieee.org/document/8060306)
<br>[[2] Advanced Building Control via Deep Reinforcement Learning](http://www.jinming.tech/papers/BuildingRL_ICAE_CR.pdf)
<br>[[3] Gao, Guanyu, Jie Li, and Yonggang Wen. "Energy-Efficient Thermal Comfort Control in Smart Buildings via Deep Reinforcement Learning." arXiv preprint arXiv:1901.04693 (2019).](https://arxiv.org/abs/1901.04693)
@snapend
+++
**Let's review some of them ...**

+++?image=/assets/img/2017-DeepReinforcementLearningforBuildingHVACControl.png&size=auto 60%&position=center
@snap[north-west]
**Deep Reinforcement Learning for Building HVAC Control [1]**
@snapend

@snap[south-west template-note text-gray]
[[1] Wei, Tianshu, Yanzhi Wang, and Qi Zhu. "Deep reinforcement learning for building HVAC control." Proceedings of the 54th Annual Design Automation Conference 2017. ACM, 2017.](https://ieeexplore.ieee.org/document/8060306)
@snapend

@snap[southeast span-40]
@box[bg-blue text-white rounded fragment](#DQN-based agent. 20% − 70% energy cost reduction compared with on-off method)
@snapend

+++?image=/assets/img/2018-AdvancedBuildingControlviaDeepReinforcementLearning.png&size=auto 70%&position=center
@snap[north-west]
**Advanced Building Control via Deep Reinforcement Learning [1]**
@snapend

@snap[south-west template-note text-gray]
[[1] 2018 - Advanced Building Control via Deep Reinforcement Learning](http://www.jinming.tech/papers/BuildingRL_ICAE_CR.pdf)
@snapend

@snap[southeast span-40]
@box[bg-blue text-white rounded fragment](#Not clear which method but the figure looks great) 
@snapend

+++?image=/assets/img/2019-Energy-EfficientThermalComfortControlinSmartBuildingsviaDeepRL_1.png&size=auto 70%&position=center
@snap[north-west]
**Energy-Efficient Thermal Comfort Control in Smart Buildings via Deep Reinforcement Learning [1]**
@snapend

@snap[south-west template-note text-gray]
[[3] Gao, Guanyu, Jie Li, and Yonggang Wen. "Energy-Efficient Thermal Comfort Control in Smart Buildings via Deep Reinforcement Learning." arXiv preprint arXiv:1901.04693 (2019).](https://arxiv.org/abs/1901.04693)
@snapend

@snap[southeast span-40]
@box[bg-blue text-white rounded fragment](#They compared DQN, DDQN, DDPG. DDPG is the best)
@snapend

+++
**Big Corp. investigating on RL-based HVAC control systems?**
<br>
AWS: [1]
<br>
IBM: [2]
<br>
Google [3]

@snap[south-west template-note text-gray]
[[1] AWSlabs.](https://github.com/awslabs/amazon-sagemaker-examples/tree/master/reinforcement_learning/rl_hvac_coach_energyplus)
<br>[[2] IBM.](https://github.com/IBM/rl-testbed-for-energyplus)
<br>[[3] Google.](https://deepmind.com/blog/deepmind-ai-reduces-google-data-centre-cooling-bill-40/)
@snapend

+++
**That's all for article review. Let's move to practice...**

---
**EnergyPlus**

+++?image=assets/img/energyplus.jpg&size=40% auto&position=left
@snap[east span-60]
@quote[EnergyPlus™ is a whole building energy simulation program that engineers, architects, and researchers use to model both energy consumption]
@snapend

+++
@snap[north-west]
**Input file**
@snapend

@snap[west span-100]
@ul[list-bullets-circles]
- IDD (input data dictionary): containing all of objects
- IDF (input data file): data describing the building
- IMF (input macro file)
- INI (initialization file): optional file for specigying the path containing .IDD
- EPW (Energy weather file): containing the hourly or sub-hourly weather data
@ulend
@snapend

+++
@snap[south-west]
**Output files**
@snapend

@snap[midpoint]
@ul[list-bullets-circles]
- err: containing the error messages
- htm/html: the option to produce data in the form of standard Web browsers
- audit:
- eso (EnergyPlus Standard Output): simulation output
- mtr (EnergyPlus Meter Output): simulation output
- mtd:  contains all the details about meters
- eio (EnergyPlus Invariant Output): containing output that does not vary with
time
- rdd: Report (variable) Data Dictionary
- mdd: Report (meter) Data Dictionary
- dxf: AutoCad™ DXF format
@ulend
@snapend

+++
@snap[south-west]
**Running EnergyPlus**
@snapend

@snap[west span-100]
show help

```bash
energyplus --help
```
![alt](assets/img/energyplus_help.png)
@snapend